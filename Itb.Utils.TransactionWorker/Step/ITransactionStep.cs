using Microsoft.EntityFrameworkCore;

namespace Itb.Utils.TransactionWorker.Step
{
    public interface ITransactionStep
    {
        DbContext GetDbContext();
        void SetDbContext(DbContext dbContext);
        void RemoveDbContext();
        dynamic Before(dynamic input);
        dynamic Execute(dynamic input);
    }
}