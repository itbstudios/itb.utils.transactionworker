using Microsoft.EntityFrameworkCore;

namespace Itb.Utils.TransactionWorker.Step
{
    public abstract class TransactionStep : ITransactionStep
    {
        private DbContext _dbContext;

        public void SetDbContext(DbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public void RemoveDbContext()
        {
            this._dbContext = null;
        }

        public DbContext GetDbContext()
        {
            return this._dbContext;
        }

        public abstract dynamic Before(dynamic input);
        public abstract dynamic Execute(dynamic input);
    }
}