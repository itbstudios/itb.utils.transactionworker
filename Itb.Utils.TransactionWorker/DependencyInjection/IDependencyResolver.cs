namespace Itb.Utils.TransactionWorker.DependencyInjection
{
    public interface IDependencyResolver
    {
        TOut Resolve<TOut>();
    }
}