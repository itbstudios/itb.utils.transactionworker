using System;

namespace Itb.Utils.TransactionWorker.DependencyInjection
{
    public static class Bootstrapper
    {
        private static IDependencyResolver _dependencyResolver;

        public static void MapResolver(IDependencyResolver dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
        }

        internal static IDependencyResolver Get()
        {
            if (_dependencyResolver == null)
            {
                throw new Exception("Before using Transactions please initialize the Dependency Resolver.");
            }

            return _dependencyResolver;
        }
    }
}