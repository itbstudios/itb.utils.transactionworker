﻿using System;
using System.Collections.Generic;
using Itb.Utils.TransactionWorker.DependencyInjection;
using Itb.Utils.TransactionWorker.Step;
using Microsoft.EntityFrameworkCore;

namespace Itb.Utils.TransactionWorker.Worker
{
    public class TransactionWorker : ITransactionWorker
    {
        private readonly string _name;
        private readonly DbContext _dbContext;
        private readonly IDependencyResolver _dependencyResolver;
        private readonly List<ITransactionStep> _transactionSteps;

        public static TransactionWorker Define(string name, DbContext dbContext)
        {
            return new TransactionWorker(name, dbContext);
        }

        private TransactionWorker(string name, DbContext dbContext)
        {
            this._name = name;
            this._transactionSteps = new List<ITransactionStep>();
            this._dependencyResolver = Bootstrapper.Get();
            this._dbContext = dbContext;
        }

        public TransactionWorker AddStep<TStep>() where TStep : ITransactionStep
        {
            var step = _dependencyResolver.Resolve<TStep>();
            this._transactionSteps.Add(step);

            return this;
        }

        public dynamic Execute(dynamic input)
        {
            try
            {
                using (_dbContext)
                {
                    this._dbContext.Database.BeginTransaction();
                    foreach (var step in _transactionSteps)
                    {
                        step.SetDbContext(this._dbContext);
                        input = step.Before(input);
                        input = step.Execute(input);
                        step.RemoveDbContext();
                    }

                    this._dbContext.Database.CommitTransaction();
                    this._dbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                input.Output = e;
            }

            return input;
        }
    }
}