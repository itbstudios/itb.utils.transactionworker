using Itb.Utils.TransactionWorker.Step;

namespace Itb.Utils.TransactionWorker.Worker
{
    public interface ITransactionWorker
    {
        TransactionWorker AddStep<TStep>() where TStep : ITransactionStep;
        dynamic Execute(dynamic input);
    }
}