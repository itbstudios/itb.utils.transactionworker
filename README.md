# Itb.Utils.TransactionWorker

#Dep Injection
First you need to create a class in your api project, for example RegisterIoc.cs with the following content
```csharp
	public class RegisterIoc : IDependencyResolver
    {
        private readonly IServiceProvider _serviceProvider;


        public RegisterIoc(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public TOut Resolve<TOut>()
        {
            return _serviceProvider.GetService<TOut>();
        }
    }
```

#Startup.cs
Now we need to let the app know about our Ioc dep resolver
1) Create new method in Startup.cs

```csharp
private void InitTransactionWorker(IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            Bootstrapper.MapResolver(new RegisterIoc(provider));
        }
```

2) Call it from ```public void ConfigureServices(IServiceCollection services)``` and the parameter is the one received by ConfigureServices method

#Controller
In your controller, on any method you have and you want to use transaction worker, follow my example (create user profile)
```csharp
		var dbContext = new dbContextHelper().CreateNewDbContext(configuration); // here you have a fresh dbContext. This will be used to start a new transaction before the first Step and commit if no exception is thrown after the last step
        dynamic context = new TransactionContext();
        context.Address = model.Address;
        context.FirstName = model.FirstName;
        context.LastName = model.LastName;
        context.PhoneNumber = model.PhoneNumber;


        var output = TransactionWorker.Define("Create Profile", dbContext)
            .AddStep<CreateProfile>()
            .Execute(context).Output;
```

```var output``` will be the result of all the steps that will be executed.

#CreateProfile step example
```csharp
	public class CreateProfile : TransactionStep
    {
        private string address;
        private DbContext dbContext;
        private string firstName;
        private string lastName;

        public override dynamic Before(dynamic input)
        {
            dbContext = (DbContext) GetDbContext();
            firstName = input.FirstName;
            lastName = input.LastName;
            address = input.Address;

            return input;
        }

        public override dynamic Execute(dynamic input)
        {
            var profile = new ProfileEntity
            {
                Address = address,
                FirstName = firstName,
                LastName = lastName,
            };

            dbContext.Profiles.Add(profile);
            input.Output = profile;
            return input;
        }
    }
```

By following this you should be able to get clean controllers, short steps classes, all steps under one fail-safe transaction.

###Enjoy!